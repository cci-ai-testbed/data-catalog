# data-catalog

data-catalog holds the set of open source and public datasets that are available by default to an AI Testbed user

Datasets loaded onto the DGX2 are located in the `/gpfs/fs1/data-catalog/ "directory_name"`. Please see the table below for further information.


## geography
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| DSTL Satellite Imagery Feature Detection | Y | N/A | Private | This datset was built to support a competition that provided 1km x 1km satellite images in 3-band and 16-band format. THe goal was for each competitor to detect and classify objects (buildings, manmade structures, road, track, trees, crops, waterway, standing water, large vehicle, and small vehicle). | J Kauffman |
| Spacenet Datasets | N | N/A | Public | Contains ~67,000 square km of very high-resolution imagery, >11M building footprints, and ~20,000 km of road labels. | J Kauffman |

## healthcare
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| CDC Dataset | Y | N/A | Private | CDC Dataset including 32 tables, 10,000 patients, and 4 CDC health catagories. | F Batarseh |

## malware
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| Sorel-20M Dataset | Y | `/gpfs/fs1/data-catalog/catalog/malware/sorel_20M` | Public | A production-scale dataset containing metadata, labels, and features for 20 million Windows Portable Executable files, including 10 million disarmed malware samples. | S Shetty |

## network_traffic
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| CTU-13 Dataset | Y | `/gpfs/fs1/data-catalog/network_traffic/CTU-13-Dataset.tar.bz2 ` | Public | A dataset of botnet traffic that was captured in the CTU University, Czech Republic, in 2011. It includes real botnet traffic mixed with normal and background traffic. | A Rahman |
| IoT Device Captures | Y | `/gpfs/fs1/data-catalog/network_traffic/captures_IoT_Sentinel.zip ` | Public | This dataset represents the traffic emitted during the setup of 31 smart home IoT devices of 27 different types. Each setup was repeated at least 20 times per device-type. | A Rahman |
| KDD Cup 1999 | Y | `/gpfs/fs1/data-catalog/network_traffic/kddcup.data.gz ` | Public | This is the data set used for The Third International Knowledge Discovery and Data Mining Tools Competition, which was held in conjunction with KDD-99 The Fifth International Conference on Knowledge Discovery and Data Mining. The competition task was to build a network intrusion detector, a predictive model capable of distinguishing between bad connections, called intrusions or attacks, and good normal connections. This database contains a standard set of data to be audited, which includes a wide variety of intrusions simulated in a military network environment. | A Rahman |
| 2017 SUEE | Y | `/gpfs/fs1/data-catalog/network_traffic/SUEE8.pcap ` | Public | the data set contains traffic in and out of the web server of the student union for Electrical Engineering at Ulm University. | A Rahman |

## social_sciences
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| Economic Data 1898-2021 | Y | N/A | Private | Economic data on production, consumption, trade, farming, and prices from 1898-2021 | F Batarseh |
| Enron Email Dataset | Y | `/gpfs/fs1/data-catalog/social_sciences/enron_mail_20150507.tar.gz ` | Public | It contains data from about 150 users, mostly senior management of Enron, organized into folders. The corpus contains a total of about 0.5M messages. | C Lu |
| Higgs Twitter Dataset | Y | `/gpfs/fs1/data-catalog/social_sciences/higgs-activity_time.txt.gz  ` | Public | The Higgs dataset has been built after monitoring the spreading processes on Twitter before, during and after the announcement of the discovery of a new particle with the features of the elusive Higgs boson on 4th July 2012. The messages posted in Twitter about this discovery between 1st and 7th July 2012 are considered. | C Lu |


## transportation
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| AssenSAR Wake Detector | Y | `/gpfs/fs1/data-catalog/transportation/sar_wakes.zip ` | Public | ship wake detection in SAR images via GMC regularization | J Kauffman |
| Stanford Drone | Y | `/gpfs/fs1/data-catalog/transportation/stanford_campus_dataset.zip ` | Public | large scale dataset that collects images and video of various types of agents (pedestrians, bicyclists, skateboarders, cars, buses, golf carts) on a college campus. | J Kauffman |

## wireless_communications
| Dataset Name | Loaded onto DGX2 | File Path | Public or Private Dataset | Dataset Description | Requested by |
| ----- | :------: | ----- | :-----: | ---------------------- | :-----: |
| 1.25GHz Localiation | Y | `/gpfs/fs1/data-catalog/wireless_communications/1-25GHz_localiation.zip ` | Public | Indoor positioning is a key enabler for a wide range of applications, including navigation, smart factories and cities, surveillance, security, IoT, and sensor networks. Additionally, indoor position can be leveraged for improved beamforming and channel estimation in wireless communications. | A Rahman |
| 5G Performance | Y | `/gpfs/fs1/data-catalog/wireless_communications/5G_performance.zip ` | Public | We conduct to our knowledge a first measurement study of commercial 5G performance on smartphones by closely examining 5G networks of three carriers (two mmWave carriers, one mid-band 5G carrier) in three U.S. cities.  | A Rahman |
| BAN Radio Channel Measurement Set | N |  | Public | This is a library of many hundreds of hours of wireless body area network (BAN) channel measurements of BAN radio channel transmit-receive link gain, featuring many different transmit-receive positions.  | A Rahman |
| Deep MIMO | Y | `/gpfs/fs1/data-catalog/wireless_communications/DeepMIMO_Dataset_Generation_v1.1.zip ` | Public | a generic deep learning dataset for millimeter wave and massive MIMO applications. | A Rahman |
| IoT Device Identification Dataset | N |  | Public | This is the main record in the IoT device measurements connected to the paper "Identification of IoT Devices using Experimental Radio Spectrum Dataset and Deep Learning". | A Rahman |
| LOG-a-TECH UWB Localization Dataset | Y | `/gpfs/fs1/data-catalog/wireless_communications/log-a-tec_data.zip ` | Public | UWB localization data set contains measurements from four different indoor environments. The data set contains measurements that can be used for range-based localization evaluation in different indoor environments. | A Rahman |
| Massive MIMO | N |  | Public | Distributed massive multiple-input multiple-output (MIMO) combines the array gain of coherent MIMO processing with the proximity gains of distributed antenna setups. | A Rahman |
| Power Allocation in Multi-Cell Massive MIMO | Y | `/gpfs/fs1/data-catalog/wireless_communications/multi_cell_massive_MIMO.zip ` | Public | Massive MIMO is a key enabler for 5G and for future dense many-user many-cell networks. Optimal design, deployment and algorithm selection for BTS and UE devices is key to efficiently using spectrum resources and maximizing the performance metrics of such systems. | A Rahman |
| RadioML | N |  | Public | A dataset which includes both synthetic simulated channel effects and over-the-air recordings of 24 digital and analog modulation types which has been heavily validated. | A Rahman |
| Rutgers/Noise | N |  | Public | We performed experiments wherein noise injection was used as a method for mapping real world wireless network topologies onto the ORBIT testbed. This dataset includes received signal strength indicator (RSSI) for each correctly received frame at receiver nodes for different levels of noise injected on the ORBIT testbed. | A Rahman |
| Sussex-Huawei Locomotion Dataset | N |  | Public | The University of Sussex-Huawei Locomotion (SHL) dataset is a versatile annotated dataset of modes of locomotion and transportation of mobile users. It was recorded over a period of 7 months in 2017 by 3 participants engaging in 8 different modes of transportation in real-life setting in the United Kingdom. | A Rahman |
| up/fr_recordings | N |  | Public | Data set of RF recordings of several communication signals captured by a real time spectrum analyzer. | A Rahman |
| ViWi | N |  | Public |a deep learning dataset framework for vision-aided wireless communications. | A Rahman |
